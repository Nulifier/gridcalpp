#pragma once

#include <Eigen/Dense>
#include <Eigen/src/Core/EigenBase.h>
#include <gridcal/types.hpp>

namespace gridcal {
    /**
     * Calcuates the power at each bus node for ZIP models.
     * @param S0 Array of nodal complex power injections.
     * @param I0 Array of nodal complex current injections.
     * @param Y0 Array of nodal complex admittance injections.
     * @param Vm Array of nodal voltage magnitudes.
     */
    template <typename R, typename A, typename B, typename C, typename D>
    Eigen::EigenBase<R> computeZipPower(const Eigen::EigenBase<A> &S0,
                                        const Eigen::EigenBase<B> &I0,
                                        const Eigen::EigenBase<C> &Y0,
                                        const Eigen::EigenBase<D> &Vm) {
	return S0 + I0 * Vm + Y0 * Vm.pow(2);
    }
}
