#pragma once

#include <complex>
#include <gridcal/gridcal-libs.hpp>

namespace gridcal {
	using cx = std::complex<double>;
	
	using mat = Eigen::MatrixXd;
	using vec = Eigen::VectorXd;
	using arr = Eigen::ArrayXd;

	using cx_mat = Eigen::MatrixXcd;
	using cx_vec = Eigen::MatrixXcd;
	using cx_arr = Eigen::ArrayXcd;
}
