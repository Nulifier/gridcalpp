cmake_minimum_required(VERSION 3.7...3.23)

if(${CMAKE_VERSION} VERSION_LESS 3.12)
	cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
endif()

project(GridCalpp
	VERSION 1.0
	DESCRIPTION "A C++ port of Santiago Peñate-Vera's Python library GridCal"
	LANGUAGES CXX
)

option(USE_OPENMP "Include support for parallelization through OpenMP" OFF)

find_package(Eigen3 REQUIRED)
find_package(OpenMP)

add_subdirectory(gridcal)

# If this is the main project and testing is enabled, build tests
if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME AND BUILD_TESTING)
	find_package(Catch2 3 REQUIRED)
	add_subdirectory(tests)
endif()
